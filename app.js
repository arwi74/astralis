// === MODULES === //
const express = require('express');
const http = require('http');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const flash = require('connect-flash');
const sync = require('sync-request');
const con = require('./app/mysql_con.js');
const session = require('express-session');
const modules = require('./app/modules.js');
const codes = require('./config/codes.json');

const router = require('./app/router.js');
const log = require('./app/log.js');
const app = express();

app.use(session({
    name: 'es',
    secret: 'faeb4453e5d14fe6f6d04637f78077c76c73d1b4',
    resave: true,
    saveUninitialized: false
}));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(flash());
app.use('/', router);

app.use(function(req, res) {
    res.status(404).send(res.render('error.ejs', codes["404"]));
    log("e", "HTTP ERROR CODE REGISTRED: 404. F000201");
});

module.exports = app;