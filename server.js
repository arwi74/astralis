const app = require('./app.js');
const log = require('./app/log.js')

app.set('port', process.env.PORT || 8080);
const server = app.listen(app.get('port'), () => {

});

module.exports = server;