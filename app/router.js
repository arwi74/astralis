const express = require('express');
const router = express.Router();
const con = require('./mysql_con.js');
const log = require('./log.js');

router.get('/', (req, res) => {
    if(req.session.logstatus == 1) {
        res.redirect('/game');
    } else {
        res.render('login.ejs');
    }
});

router.get('/game', (req, res) => {
    if(req.session.logstatus == 1) {
        con.query("SELECT nickname, balance, level, pz, sp, exp, maxpz, maxsp, maxexp, pos FROM as_users WHERE id='"+req.session.userid+"'", (e, r) => {
            if(e) { 
                log("e", "Error with database! Code: F000101")
            } else {
                var data = {};
                data.user = r[0];
                res.render('index.ejs', data);
            }
        });
    } else {
        res.redirect('/');
    }
});


module.exports = router;