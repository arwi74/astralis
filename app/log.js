const colors = require("colors");
function log(type, val) {
    var date = new Date();
    var sc = date.getSeconds();
    var mi = date.getMinutes();
    var ho = date.getHours();
    if(sc < 10) {
        sc = "0"+sc;
    }
    if(mi < 10) {
        mi = "0"+sc;
    }
    if(ho < 10) {
        ho = "0"+ho;
    }

    var d = ""+ho+":"+mi+":"+sc+"";
    if(type == "e") {
        console.log(colors.red("[!] "+d+" | "+val));
    } else if(type == "i") {
        console.log(colors.yellow("[i] "+d+" | "+val));
    } else if(type == "+") {
        console.log(colors.green("[+] "+d+" | "+val));
    } else if(type == "d") {
        console.log(colors.cyan("[D] "+d+" | "+val));
    } else if(type == "-") {
        console.log(colors.red("[-]  "+d+" | "+val));
    }
}

module.exports = log;