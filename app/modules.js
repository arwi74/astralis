const log = require('./log.js');
const server = require('../server.js');
const con = require('./mysql_con.js');
const text = {
    margin: "     ",
    newLine: "\n     "
};

require('./post.js');
require('./errorControler.js');

log("i", "All files exists.");
log("i", "Modules loaded succesfull.");
log("i", "Starting serwer...");
log("+", "Serwer stared!"+text.newLine+"(Port: 8080, Host: 127.0.0.1, Owner: root, Version: 0.1a, Build Type: Development)");
con.connect(function(err) {
    if (err) {
        log("e", "Error with connecting to database! Code: F000001"+text.newLine+"("+err+")");
    } else {
        log("+", "Connected with database.");
    }
});