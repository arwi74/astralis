const router = require('./router.js');
const crypto = require('crypto');
const con = require('./mysql_con.js');
const log = require('./log.js');

router.post("/", (req, res) => {
    var login = req.body.login;
    var password = crypto.createHash('md5').update(req.body.password).digest("hex");
    con.query("SELECT * FROM as_users WHERE login='"+login+"'AND password='"+password+"'", (e, r) => {
        if(e) {
            res.redirect('/');
        } else {
            if(r != null) {
                req.session.logstatus = 1;
                req.session.userid = r[0].id;
                res.redirect('/game');
            }
        }
    });
});

    /* Przekierowania lokalizacji */

router.get('/przenies/miasto', (req, res) => {
    if(req.session.logstatus == 1) {
        con.query("UPDATE as_users SET pos=1 WHERE id='"+req.session.userid+"'", (e) => {
            if(e) {
                log("e", "Error with database! Code: F000102");
            } else {
                res.redirect('/game');
            }
        });
    } else {
        res.redirect('/');
    }
});

router.get('/przenies/las', (req, res) => {
    if(req.session.logstatus == 1) {
        con.query("UPDATE as_users SET pos=2 WHERE id='"+req.session.userid+"'", (e) => {
            if(e) {
                log("e", "Error with database! Code: F000103");
            } else {
                res.redirect('/game');
            }
        });
    } else {
        res.redirect('/');
    }
});

    /* Koniec Przekierowania lokacji */

    /* Przekierowania drzwi */

router.get('/przenies/miasto/tawerna', (req, res) => {
    if(req.session.logstatus == 1) {
        con.query("UPDATE as_users SET pos=3 WHERE id='"+req.session.userid+"'", (e) => {
            if(e) {
                log("e", "Error with database! Code: F000104");
            } else {
                res.redirect('/game');
            }
        });
    } else {
        res.redirect('/');
    }
});

    /* Koniec Przekierowania drzwi */

    /* Akcje w lokacjach */

router.get('/przenies/miasto/tawerna/pijpiwo', (req, res) => {
    if(req.session.logstatus == 1) {
        con.query("UPDATE as_users SET pos=3 WHERE id='"+req.session.userid+"'", (e) => {
            if(e) {
                log("e", "Error with database! Code: F000301");
            } else {
                res.redirect('/game');
            }
        });
    } else {
        res.redirect('/');
    }
});

    /* Koniec akcji w lokacjach */

router.get('/logout', (req, res) => {
    req.session.logstatus = 0;
    req.session.userid = null;
    res.redirect('/');
});